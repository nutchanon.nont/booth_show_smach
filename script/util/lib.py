#!/usr/bin/env python

import rospy
import smach
import smach_ros
import time
import os
from std_srvs.srv import Empty
from std_msgs.msg import String
# from skuba_nav_lib.navigation import Navigation
from skuba_athome_msgs.srv import SpeakCommand

class Prepared(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'])
    
    def execute(self,userdata):
        rospy.wait_for_service("/speech/open_mic")
        rospy.wait_for_service("/speech/close_mic")

        rospy.logwarn('Executing STATE : Prepared ')
        return 'finish'


class ListenNoTimeout(smach.State):
    def __init__(self,sentence):
        smach.State.__init__(self, outcomes=['finish'])
        self.sentence = sentence
        rospy.Subscriber('/speech/thai', String, self.listen_callback)
        self.close_mic = rospy.ServiceProxy('/speech/close_mic', Empty)
        self.open_mic = rospy.ServiceProxy('/speech/open_mic', Empty)

    def listen_callback(self,data):
        self.word = data.data

    def execute(self,userdata):
        self.word = ''
        self.open_mic()
        while not rospy.is_shutdown():
            if self.word == self.sentence:
                break
        self.close_mic()
        rospy.logwarn('Executing STATE : Listen No Timeout for ' + self.sentence)
        return 'finish'


class Talk(smach.State):
    def __init__(self, sentence):
        smach.State.__init__(self, outcomes=['finish'])
        self.sentence = sentence
        self.speak = rospy.ServiceProxy("/speech/speak_thai",SpeakCommand)
        self.close_mic = rospy.ServiceProxy('/speech/close_mic', Empty)

    def execute(self,userdata):
        self.close_mic()
        while(self.speak(self.sentence)==None):
            pass
        rospy.logwarn('Executing STATE : Talk '+self.sentence)
        return 'finish'


class ListenForLocation(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['finish',"timeout"], output_keys=['location'])
        rospy.Subscriber('/speech/thai', String, self.listen_callback)
        self.open_mic = rospy.ServiceProxy('/speech/open_mic', Empty)
        self.close_mic = rospy.ServiceProxy('/speech/close_mic', Empty)

    def listen_callback(self,data):
        self.location = data.data
    
    def execute(self, userdata):
        self.location = ''
        fututre = time.time() + 10
        self.open_mic()
        while not rospy.is_shutdown():
            if self.location != '':
                break
            if time.time() > fututre:
                self.close_mic()
                return 'timeout'
        self.close_mic()
        userdata.location = self.location
        rospy.logwarn('Executing STATE : ListenForLocation ')
        return 'finish'


class TalkLocation(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'], input_keys=['location'])
        self.speak = rospy.ServiceProxy("/speech/speak_thai",SpeakCommand)
        self.close_mic = rospy.ServiceProxy('/speech/close_mic', Empty)
    
    def execute(self,userdata):
        self.close_mic()
        self.sentence = ''
        self.sentence = "คุณต้องการไปที่ " + str(userdata.location) + "ใช่หรือไม่"
        while(self.speak(self.sentence)==None):
            pass
        rospy.logwarn('Executing STATE : Talk '+self.sentence)
        return 'finish'


class RecheckLocation(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=["correct","incorrect","timeout"], input_keys=["location"])
        self.speak = rospy.ServiceProxy("/speech/speak_thai",SpeakCommand)
        rospy.Subscriber('/speech/thai', String, self.listen_callback)
        self.open_mic = rospy.ServiceProxy('/speech/open_mic', Empty)
        self.close_mic = rospy.ServiceProxy('/speech/close_mic', Empty)
    
    def listen_callback(self,data):
        self.word = data.data
    
    def execute(self, userdata):
        self.word = ''
        self.sentence = userdata.location
        fututre = time.time() + 10
        # rospy.sleep(1)
        self.open_mic()
        while not rospy.is_shutdown():
            if self.word == "ใช่":
                self.close_mic()
                rospy.logwarn('Executing STATE : RecheckLocation '+self.sentence)
                return "correct"
            elif self.word == "ไม่":
                self.close_mic()
                rospy.logwarn('Executing STATE : RecheckLocation '+self.sentence)
                return "incorrect"
            if time.time() > fututre:
                self.close_mic()
                rospy.logwarn('Executing STATE : RecheckLocation '+self.sentence)
                return 'timeout'


class TalkgoingtoLocation(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'], input_keys=['location'])
        self.speak = rospy.ServiceProxy("/speech/speak_thai",SpeakCommand)
        self.close_mic = rospy.ServiceProxy('/speech/close_mic', Empty)
    
    def execute(self,userdata):
        self.close_mic()
        self.sentence = ''
        self.sentence = "ฉันกำลังจะพาคุณไปที่ " + str(userdata.location) 
        while(self.speak(self.sentence)==None):
            pass
        rospy.logwarn('Executing STATE : Talk '+self.sentence)
        return 'finish'


class Nav(smach.State):

    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'], input_keys=['location'])
        pass

    def execute(self,userdata):
        self.location = str(userdata.location)
        print("Went to "+ self.location)
        rospy.logwarn("Executing STATE : go to "+self.location)
        return 'finish'

    '''
    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'], input_keys=['location'])
        self.nav = Navigation("/home/skuba/skuba_ws/src/skuba_athome_smach/script/receptionist/util/file.csv")
        self.clear_map = rospy.ServiceProxy("/move_base/clear_costmaps",Empty)

    def execute(self,userdata):
        self.location = str(userdata.location)
        if self.location == "go_to_livingroom":
            self.nav.go_to_position("living room")
            if(self.nav.get_state(60)):
                  pass
            else:
                self.clear_map()
                self.nav.go_to_position("living room")

        elif self.location == "go_to_the_door":
            self.nav.go_to_position("start pos")
            if(self.nav.get_state(60)):
                pass
            else:
                self.clear_map()
                self.nav.go_to_position("start pos")

        rospy.logwarn("Executing STATE : go to "+self.location)
        return 'finish'
    '''


class Talk_arrive_at_location(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'], input_keys=['location'])
        self.speak = rospy.ServiceProxy("/speech/speak_thai",SpeakCommand)
        self.close_mic = rospy.ServiceProxy('/speech/close_mic', Empty)
    
    def execute(self,userdata):
        self.close_mic()
        self.sentence = ''
        self.sentence = "ฉันได้พาคุณมาถึง" + str(userdata.location) + "แล้ว"
        while(self.speak(self.sentence)==None):
            pass
        rospy.logwarn('Executing STATE : Talk '+self.sentence)
        return 'finish'


class GotoHome(smach.State):

    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'])

    def execute(self,userdata):
        print("Went to ")
        rospy.logwarn("Executing STATE : go to ")
        return 'finish'

    '''
    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'])
        self.nav = Navigation("/home/skuba/skuba_ws/src/skuba_athome_smach/script/receptionist/util/file.csv")
        self.clear_map = rospy.ServiceProxy("/move_base/clear_costmaps",Empty)

    def execute(self):
        self.nav.go_to_position("Station")
        if(self.nav.get_state(60)):
                pass
        else:
            self.clear_map()
            self.nav.go_to_position("x")

        rospy.logwarn("Executing STATE : go to home")
        return 'finish'
    '''