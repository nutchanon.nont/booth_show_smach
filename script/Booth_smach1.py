#!/usr/bin/env python

import rospy
import smach
import smach_ros
from util.lib import *

########################################################################################
###################################### M A I N #########################################
########################################################################################

def main():
    rospy.init_node('Booth_state_machine')
    sm = smach.StateMachine(outcomes=['----STATE END----'])
    sm.userdata.location = ""

    with sm:
        # smach.StateMachine.add('prepared', Prepared(),
        #                             transitions={'finish':'wait_for_name_call'})
        smach.StateMachine.add('wait_for_name_call', ListenNoTimeout("นำทางฉัน"), #angus นำทางฉัน
                                    transitions={'finish':'ask_location'})
        # smach.StateMachine.add('talk_to_people', TalktoPeople(),
        #                             transitions={'finish':'wait_for_name_call','asked_to_Lead':'ask_location'})
        smach.StateMachine.add('ask_location', Talk('คุณต้องการไปที่ไหน'),
                                    transitions={'finish':'listen_for_location'})
        smach.StateMachine.add('timeout_ask_location_again', Talk('ขออภัย, ฉันไม่ได้ยิน, โปรดพูดอีกทีหลังเสียงติ๊ด'),
                                    transitions={'finish':'listen_for_location'})
        smach.StateMachine.add('listen_for_location', ListenForLocation(),
                                    transitions={'finish':'say_location','timeout':'timeout_ask_location_again'},
                                    remapping={'location':'location'})
        smach.StateMachine.add('say_location', TalkLocation(),
                                    transitions={'finish':'recheck_location'},
                                    remapping={'location':'location'})
        smach.StateMachine.add('recheck_location', RecheckLocation(),
                                    transitions={'correct':'say_to_location',"incorrect":"ask_location","timeout":"timeout_recheck_location"},
                                    remapping={"location":"location"})
        smach.StateMachine.add('timeout_recheck_location', Talk('ขออภัย, ฉันไม่ได้ยิน, โปรดพูดใช่หรือไม่อีกทีหลังเสียงติ๊ด'),
                                    transitions={'finish':'recheck_location'})
        smach.StateMachine.add('say_to_location', TalkgoingtoLocation(),
                                    transitions={'finish':'go_to_location'},
                                    remapping={"location":"location"})
        smach.StateMachine.add('go_to_location', Nav(),
                                    transitions={'finish': 'tell_arrive'},
                                    remapping={"location":"location"})
        smach.StateMachine.add('tell_arrive', Talk_arrive_at_location(),
                                    transitions={'finish': 'listen_to_thank_you'},
                                    remapping={"location":"location"})
        smach.StateMachine.add('listen_to_thank_you', ListenNoTimeout("ขอบคุณ"), #ต้องแยก ครับ/ค่ะ ไหม?
                                    transitions={'finish':'say_going_back'})
        smach.StateMachine.add('say_going_back', Talk("ฉันกำลังจะกลับไปที่บูธ, ขอบคุณค่ะ"),
                                    transitions={'finish':'Go_back_Home'})
        smach.StateMachine.add('Go_back_Home', GotoHome(),
                                    transitions={'finish':'Say_arrive_Home'})
        smach.StateMachine.add('Say_arrive_Home', Talk("ฉันได้ถึงที่บูธแล้ว"),
                                    transitions={'finish':'wait_for_name_call'})
    
    # sis = smach_ros.IntrospectionServer('CARRY_MY_LUGGAGE', carry_my_luggage, '/SM_ROOT')
    # sis.start()

    # Execute the state machine
    # outcome = listen_and_detection.execute()
    outcome = sm.execute()

    # Wait for ctrl-c to stop the application
    rospy.spin()
    sis.stop()


if __name__ == '__main__':
    main()

                                    
